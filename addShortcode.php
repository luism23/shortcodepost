<?php
/*
Plugin Name: Shortcode Post
Plugin URI: https://gitlab.com/luism23/shortcodepost
Description: Add shorcode for show post, use [SHPOS_showPost].
Author: LuisMiguelNarvaez
Version: 1.0.3
Author URI: https://gitlab.com/luism23
License: GPL
 */

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/luism23/shortcodepost',
	__FILE__,
	'shortcode-post'
);


define("SHPOS_DIR",plugin_dir_path( __FILE__ ));
define("SHPOS_URL",plugin_dir_url( __FILE__ ));

require_once  SHPOS_DIR . 'src/index.php';